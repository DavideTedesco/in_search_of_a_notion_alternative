# In search of a _Notion_ alternative

Repository born to find a **[Notion](https://www.notion.so) Open Source alternative**...

## Self-hosted alternatives

- [BookStack](https://www.bookstackapp.com/)
- [Wiki.js](https://js.wiki/)
- [Quip](https://quip.com/) #works offline

## Open Source wikis

- [TiddlyWiki](https://tiddlywiki.com/)
- [Zim](https://www.zim-wiki.org/)
- [CherryTree](https://www.giuspen.com/cherrytree/)
- [Joplin](https://joplinapp.org/)
- [Confluence](https://www.atlassian.com/software/confluence)
- [Standard Notes](https://standardnotes.com/)
- [Outline](https://www.getoutline.com) #self-hosted
- [Logseq](https://logseq.com/)
- [AppFlowy](https://appflowy.io/) suggested by @FrancescoMartino
- [Anytype](https://anytype.io/) suggested by @buonhobo

## Non Open Source options

- [craft.io](https://craft.io/)
- [Obsidian](https://obsidian.md/)
  - [Hack your brain with Obsidian.md](https://www.youtube.com/watch?v=DbsAQSIKQXk&ab_channel=NoBoilerplate)
- [Asana](https://asana.com/)
- [NoteLedge](https://www.kdanmobile.com/noteledge)
- [Quire](https://quire.io/)
- [Roam Research](https://roamresearch.com/)
- [nTask](https://www.ntaskmanager.com/)
- [Coda](https://coda.io/welcome)
- [Tettra](https://tettra.com/)
- [ClickUp](https://clickup.com/)
- [Nimbus Note](https://nimbusweb.me/note.php)
- [Slab](https://slab.com/)
- [NoteLedge](https://www.kdanmobile.com/noteledge)


## Resources
[12 Best Notion Alternatives to Use in 2021](https://www.ntaskmanager.com/blog/best-notion-alternatives/#nTask)
[Notion alternatives on alternativeto.net](https://alternativeto.net/software/notion/)
